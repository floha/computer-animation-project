# NotPong

# What's done:

- basic principle like Pong
- all in 3D: field, paddles, ball, walls
- everything floating in space, background sphere
- camera slightly follows ball, creating a more 3D view
- wall behind players get smashed on ball collision
- ball speed increases over time (to be precise, on each paddle hit)
- alien spawns after 10-20 secs, floating around in the middle of the field
- alien grabs ball on collision and throws it away in a slightly random direction

# What needs to be done:

- already existing alien idle animation looped while moving around
- alien should move more fluently and look into movement direction
- sound effects / music
- better/more textures, better lighting
- ...
