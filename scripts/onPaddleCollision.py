from math import sqrt

# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    if globalDict["justMirroredX"] is False:
        ball = getCurrentScene().objects["ball"]
        if ball.position.x < 0:
            paddle = getCurrentScene().objects["bluePaddle"]
        else:
            paddle = getCurrentScene().objects["redPaddle"]

        # if the paddle is hit in the middle, the angle is 0 degrees
        # a positive angle means the ball y location is greater than the paddle's
        # therefore, the y speed of the ball should be positive after the collision
        y_diff = ball.position.y - paddle.position.y
        y_angle = min(45 * y_diff, 60) if y_diff >= 0 else max(45 * y_diff, -60)

        current_speed = sqrt(pow(globalDict["ballSpeedX"], 2) + pow(globalDict["ballSpeedY"], 2))
        new_speed = current_speed * 1.01

        new_x_direction = -1 if globalDict["ballSpeedX"] >= 0 else 1
        new_y_direction = 1 if y_angle >= 0 else -1

        speed_y_part = abs(y_angle) / 90
        speed_x_part = 1 - speed_y_part

        # we need to calculate a speed factor to preserve the current speed
        # current_speed = sqrt((factor * speed_x_part)^2 + (factor * speed_y_part)^2)
        # factor = (+/-) current_speed / sqrt(speed_x_part^2 + speed_y_part^2)
        speed_factor = new_speed / sqrt(pow(speed_x_part, 2) + pow(speed_y_part, 2))

        globalDict["ballSpeedX"] = speed_x_part * speed_factor * new_x_direction
        globalDict["ballSpeedY"] = speed_y_part * speed_factor * new_y_direction

        globalDict["justMirroredX"] = True
    else:
        globalDict["justMirroredX"] = False


main()
