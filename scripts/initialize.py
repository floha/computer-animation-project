from random import randint
from time import time

# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    globalDict["startTime"] = time()
    globalDict["gameOver"] = False
    globalDict["gameOver"] = False

    globalDict["ballSpeedX"] = 0.15
    globalDict["ballSpeedY"] = 0

    globalDict["camBasePositionY"] = -22

    globalDict["paddleMinY"] = -7.3
    globalDict["paddleMaxY"] = 7.3
    globalDict["ballSpeed"] = 0.3

    globalDict["justMirroredX"] = False
    globalDict["justMirroredY"] = False

    globalDict["alienSpawnTime"] = globalDict["startTime"] + randint(10, 20)
    globalDict["alienSpawned"] = False
    globalDict["alienPosMaxX"] = 5
    globalDict["alienPosMinX"] = -5
    globalDict["alienPosMaxY"] = 5
    globalDict["alienPosMinY"] = -5
    globalDict["alienSpeedX"] = 0.03
    globalDict["alienSpeedY"] = 0.03
    globalDict["alienRandSpeedMin"] = 0.02
    globalDict["alienRandSpeedMax"] = 0.05

    globalDict["ballGrabbedCount"] = 0
    globalDict["ballImmuneCount"] = 0


main()
