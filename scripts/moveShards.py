# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    if globalDict["gameOver"]:
        objects = getCurrentScene().objects
        for obj in objects:
            if "Wall_cell." in obj.name:
                speed = globalDict["wall_cell_speeds"][obj]
                obj.position.x += speed[0]
                obj.position.y += speed[1]
                obj.position.z += speed[2]


main()
