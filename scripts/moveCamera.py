# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    if not globalDict["gameOver"]:
        camera = getCurrentScene().objects["camera"]
        ball = getCurrentScene().objects["ball"]
        camera.position.x = ball.position.x / 10
        camera.position.y = globalDict["camBasePositionY"] + ball.position.y / 15


main()
