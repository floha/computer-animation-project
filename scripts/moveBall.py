# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    if not globalDict["gameOver"]:
        ball = getCurrentScene().objects["ball"]
        ball.applyRotation([0, 0, 0.02])
        if globalDict["ballGrabbedCount"] <= 0:
            ball.position.x += globalDict["ballSpeedX"]
            ball.position.y += globalDict["ballSpeedY"]


main()
