# noinspection PyUnresolvedReferences
from bge.logic import getCurrentController, globalDict


def main():
    if not globalDict["gameOver"]:
        controller = getCurrentController()
        paddle = controller.owner
        current_y = paddle.position.y

        move_left = controller.sensors['moveLeft'].positive
        move_right = controller.sensors['moveRight'].positive

        if move_left and not move_right:
            new_y = min(globalDict["paddleMaxY"], current_y + globalDict["ballSpeed"])
            paddle.position.y = new_y

        elif move_right and not move_left:
            new_y = max(globalDict["paddleMinY"], current_y - globalDict["ballSpeed"])
            paddle.position.y = new_y


main()
