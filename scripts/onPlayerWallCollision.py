import math

# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def get_shard_speed(shard_position, origin_point):
    """
    returns a tuple of the form (x_speed, y_speed, z_speed)
    origin should also have that form and, in the easiest case, is the center
    of the ball at the time of collision
    """
    # generate the total speed of a shard based on the distance to the origin
    # |(dx, dy, dz)| = sqrt(dx**2 + dy**2 + dz**2)
    distance = math.sqrt(sum([(sp - op) * (sp - op) for sp, op in zip(shard_position, origin_point)]))
    max_speed = 0.1
    min_speed = 0.05
    distance_factor = 1
    total_speed = (max_speed - min_speed) * math.exp(-distance_factor * distance) + min_speed
    # calculate the x-, y-, and z-speeds
    # vector from origin to shard
    vec = [sp - op for sp, op in zip(shard_position, origin_point)]
    # normalize and multiply with total_speed to get a length of total_speed
    vec = [component / distance * total_speed for component in vec]
    return vec


def main():
    globalDict["gameOver"] = True

    scene = getCurrentScene()
    objects = scene.objects

    # check which player is losing
    if objects["ball"].position.x < 0:
        losing_wall = "blueWall"
    else:
        losing_wall = "redWall"

    objects[losing_wall].endObject()

    # store wall shard speeds
    globalDict["wall_cell_speeds"] = dict()

    # show the losing player wall shards
    ball_position = objects["ball"].position
    origin_of_collision = [ball_position.x * 3 / 4, ball_position.y, ball_position.z]

    search_str = losing_wall + "_cell."
    for obj in scene.objectsInactive:
        if search_str in obj.name:
            added = scene.addObject(obj, obj)
            added.suspendDynamics()
            shard_position = [
                added.position.x,
                added.position.y,
                added.position.z
            ]
            globalDict["wall_cell_speeds"][added] = get_shard_speed(
                shard_position, origin_of_collision)

    objects["ball"].endObject()


main()
