import random
from time import time

# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    if not globalDict["gameOver"] and \
            not globalDict["alienSpawned"] and \
            time() > globalDict["alienSpawnTime"]:
        scene = getCurrentScene()
        inactive_alien = scene.objectsInactive["alien"]
        alien = scene.addObject(inactive_alien, inactive_alien)
        globalDict["alienSpawned"] = True
        alien.position.x = random.uniform(globalDict["alienPosMinX"], globalDict["alienPosMaxX"])
        alien.position.y = random.uniform(globalDict["alienPosMinY"], globalDict["alienPosMaxY"])


main()
