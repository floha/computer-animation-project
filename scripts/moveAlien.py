import random

# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    # decrement grab status counter
    if globalDict["ballGrabbedCount"] > 0:
        globalDict["ballGrabbedCount"] -= 1
    if globalDict["ballImmuneCount"] > 0:
        globalDict["ballImmuneCount"] -= 1

    if not globalDict["gameOver"] and globalDict["alienSpawned"] and globalDict["ballGrabbedCount"] <= 0:
        alien = getCurrentScene().objects["alien"]

        if alien.position.x < globalDict["alienPosMinX"]:
            new_x_direction = 1
        elif alien.position.x > globalDict["alienPosMaxX"]:
            new_x_direction = -1
        else:
            new_x_direction = None

        if alien.position.y < globalDict["alienPosMinY"]:
            new_y_direction = 1
        elif alien.position.y > globalDict["alienPosMaxY"]:
            new_y_direction = -1
        else:
            new_y_direction = None

        if new_x_direction is not None or new_y_direction is not None:
            # need to randomize missing direction (in case there is one)
            new_x_direction = new_x_direction if new_x_direction is not None else (1 if random.random() < 0.5 else -1)
            new_y_direction = new_y_direction if new_y_direction is not None else (1 if random.random() < 0.5 else -1)

            # randomize new speed values
            new_x_speed = random.uniform(globalDict["alienRandSpeedMin"], globalDict["alienRandSpeedMax"])
            new_y_speed = random.uniform(globalDict["alienRandSpeedMin"], globalDict["alienRandSpeedMax"])

            globalDict["alienSpeedX"] = new_x_direction * new_x_speed
            globalDict["alienSpeedY"] = new_y_direction * new_y_speed

        alien.position.x += globalDict["alienSpeedX"]
        alien.position.y += globalDict["alienSpeedY"]


main()
