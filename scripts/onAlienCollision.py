import random
from math import sqrt

# noinspection PyUnresolvedReferences
from bge.logic import globalDict, getCurrentScene


def main():
    if globalDict["ballImmuneCount"] <= 0:
        globalDict["ballGrabbedCount"] = random.randint(40, 120)
        globalDict["ballImmuneCount"] = globalDict["ballGrabbedCount"] + 10

        objects = getCurrentScene().objects
        ball = objects["ball"]
        alien = objects["alien"]

        out_vec = (
            (ball.position.x - alien.position.x) * random.uniform(0.6, 1.4),
            (ball.position.y - alien.position.y) * random.uniform(0.6, 1.4),
        )
        y_factor = abs(out_vec[1] / out_vec[0])

        speed = sqrt(pow(globalDict["ballSpeedX"], 2) + pow(globalDict["ballSpeedY"], 2))
        print("speed before:", speed)

        # speed^2 = x_speed^2 + (x_speed * y_factor)^2
        # x_speed = (+/-) speed / sqrt(y_factor^2 + 1)
        x_speed = speed / sqrt(pow(y_factor, 2) + 1)
        y_speed = x_speed * y_factor

        globalDict["ballSpeedX"] = x_speed * (1 if out_vec[0] >= 0 else -1)
        globalDict["ballSpeedY"] = y_speed * (1 if out_vec[1] >= 0 else -1)

        speed = sqrt(pow(globalDict["ballSpeedX"], 2) + pow(globalDict["ballSpeedY"], 2))
        print("speed after:", speed)


main()
