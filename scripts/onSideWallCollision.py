# noinspection PyUnresolvedReferences
from bge.logic import globalDict


def main():
    if globalDict["justMirroredY"] is False:
        globalDict["ballSpeedY"] = -globalDict["ballSpeedY"]

        globalDict["justMirroredY"] = True
    else:
        globalDict["justMirroredY"] = False


main()
